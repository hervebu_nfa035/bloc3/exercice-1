package fr.cnam.foad.nfa035.badges.testgui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AppH {
    private JButton buttonMsg;
    private JPanel panelMain;

    public AppH() {
        buttonMsg.setAlignmentX(5);
        buttonMsg.setSize(6,7);
        buttonMsg.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               JOptionPane.showMessageDialog(null,"Hello");
            }
        });
    }

    public static void main(String[] args) {

        JFrame frame = new JFrame("AppH");
        frame.setContentPane(new AppH().panelMain);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
