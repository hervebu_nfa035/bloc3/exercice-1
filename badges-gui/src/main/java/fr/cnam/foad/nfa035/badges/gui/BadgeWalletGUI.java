package fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.testgui.AppH;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class BadgeWalletGUI {
    private JButton button1;
    private JPanel panel1;
    private JTable table1;
    private static final String RESOURCES_PATH = "badges-gui/src/test/resources/";

    /**
     * constructeur :
     * Création d'un tableau et d'un bouton.
     * Récupération de la liste des batches classés
     * affichage dans un tableau en utilisant un modéle et en appliquant des options de rendu
     * dépendant du contenu des données.
     */
    public BadgeWalletGUI () {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Bien joué!");
            }
        });
        DirectAccessBadgeWalletDAO dao = null;
        File f = new File("text.txt");
        System.out.println( "chemin de f " + f.getAbsolutePath());
        try {
            dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
            Set<DigitalBadge> metaSet = dao.getWalletMetadata();
            List<DigitalBadge> tableList = new ArrayList<>();
            Iterator<DigitalBadge> it = new TreeSet(metaSet).iterator();
            while (it.hasNext())
                tableList.add(it.next());
            // tableList.addAll(metaSet);
            // Ci dessous ligne utilisée pour la premiere partie de l'exercice
            // TableModel tableModel = TableModelCreator.createTableModel(DigitalBadge.class, tableList);
            BadgesModel tableModel = new BadgesModel(tableList);
            // appeller les outils de rendu
            table1.setModel(tableModel);
            table1.getColumnModel().getColumn(4).setCellRenderer(new BadgeSizeCellRenderer());
            // table1.setDefaultRenderer(Number.class, new DefaultBadgeCellRenderer());
            // traitement particulier des dates
            table1.setDefaultRenderer(Date.class, new DefaultBadgeCellRenderer());


        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    /**
     *
     * main de la classe permettant de l'exécuter :
     * déclaration du frame permettant de visualiser un frame à partir
     * de la création d'une nouvelle instance de BadgeWalletGUI
     * @param args
     */
    public static void main(String[] args) {

        JFrame frame = new JFrame("My Badge Wallet");
        frame.setContentPane(new BadgeWalletGUI().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
