package fr.cnam.foad.nfa035.badges.gui;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

/**
 *
 */
public class BadgeSizeCellRenderer extends DefaultTableCellRenderer {
    private Color originBackground;
    private Color originForeground;
    private final Long MAX_SIZE = 10000L;

    public BadgeSizeCellRenderer()
    {
        super();
        originBackground = getBackground();
        originForeground = getForeground();
    }

    /**
     * permet de déterminer la couleur du fond et celle des caracteres.
     * La couleur du fond sera Orange si la taille est plus grande que MAX_SIZE (10000)
     * La couleur des caracteres sera rouge si la ligne est selectionnée
     * @param table le composant JTable
     *      * @param value : contenu à afficher dans la cellule
     *      * @param isSelected : sélectionné
     *      * @param hasFocus Si focus
     *      * @param row numero de  ligne
     *      * @param column numero de colonne
     *      * @return Composant
     *
     */
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus, row, column);
        Long badgeSize = (Long) value;
        setText(humanReadableByteCountBin(badgeSize));
        System.out.println( row + "selected" + isSelected);


        setForeground(this.originBackground);
        if (isSelected) {

            if (badgeSize > MAX_SIZE)
                setForeground(Color.RED);

        }
        else {
            setBackground(this.originBackground);
            if (badgeSize > MAX_SIZE)
                setBackground(Color.ORANGE);
        }
        return component;
    }

    private String humanReadableByteCountBin(long bytes) {
            long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
            if (absB < 1024) {
                return bytes + " o";
            }
            long value = absB;
            CharacterIterator ci = new StringCharacterIterator("KMGTPE");
            for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
                value >>= 10;
                ci.next();
            }
            value *= Long.signum(bytes);
            return String.format("%.1f %co", value / 1024.0, ci.current());
        }




}
