package fr.cnam.foad.nfa035.badges.gui;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.util.Date;

public class DefaultBadgeCellRenderer extends DefaultTableCellRenderer {
    private Color originForeground = Color.black;

    public DefaultBadgeCellRenderer() {
        super();
        this.originForeground = getForeground();
    }
    /**
     * permet de gérer les critéres de rendu (couleur du fon et couleur des caracteres ....)
     * @param table le composant JTable
     * @param value : contenu à afficher dans la cellule
     * @param isSelected : sélectionné (B
     * @param hasFocus Si focus
     * @param row numero de  ligne
     * @param column numero de colonne
     * @return Composant
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus,    row, column);

        if (((Date)table.getModel().getValueAt(row, 3)).before(new Date())){
            setForeground(Color.RED);
        }

        return comp;
    }
}
