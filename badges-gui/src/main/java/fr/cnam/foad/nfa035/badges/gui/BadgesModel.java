package fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.swing.table.AbstractTableModel;
import java.util.Date;
import java.util.List;

public class BadgesModel extends AbstractTableModel {
    private List<DigitalBadge> badges;
    private final String[] entetes = { "ID " , "Code Série" , "Début" , "fin ", "Taille(octets)"};
    private long serialVersionUID = 1L;

    public BadgesModel (List<DigitalBadge> badgesList) {
        badges = badgesList;
    }

    public List<DigitalBadge> getBadges() {
        return badges;
    }

    /**
     * obtenir nombre de colonnes utiles du tableau
     *
     * @return : nombre de colonnes
     */
    public int getColumnCount() {
        return entetes.length;
    }

    /**
     * obtenir nombre de lignes
     * @return
     */
    @Override
    public int getRowCount() {
        return badges.size();
    }

    /**
     * cette methode permet de conseiller la classe de donnée à utiliser pour formatter la donnée
     * @param columnIndex
     * @return : classe de la donnée à afficher dans la cellule
     */
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Integer.class;
            case 1:
                return String.class;

            case 2:
            case 3:
                return Date.class;

            case 4:
                return Double.class;

            default:
                return Object.class;
        }


    }

    /**
     * obtenir l'entete de la colonne
     * @param columnIndex
     */
    public String getColumnName (int columnIndex) {
        return entetes[columnIndex];
    }
    /**
     * obtenir valeur de chaque cellule du tableau
     * @param rowIndex   : ligne index
     * @param columnIndex : colonne index
     * @return : contenu de la cellule
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                // ID (rang dans le wallet)
                return badges.get(rowIndex).getMetadata().getBadgeId();
            case 1:
                // Serial (Numero de serie)
                return badges.get(rowIndex).getSerial();

            case 2:
                // begin (Début de validité)
                return badges.get(rowIndex).getBegin();
            case 3:
                // begin (Début de validité)
                return badges.get(rowIndex).getEnd();
            case 4:
                // Taille du badge (octets)
                return badges.get(rowIndex).getMetadata().getImageSize();
        }
        throw new IllegalArgumentException();
    }
}
