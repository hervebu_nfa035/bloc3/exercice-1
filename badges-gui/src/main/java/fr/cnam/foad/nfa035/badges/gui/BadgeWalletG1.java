package fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class BadgeWalletG1 {
    private static final String RESOURCES_PATH = "src/test/resources/";
    JButton button1;
    JPanel jPanel;
    JTable table1;
    public BadgeWalletG1() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Bien joué!");
            }
        });
        DirectAccessBadgeWalletDAO dao = null;
        try {
            dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
            Set<DigitalBadge> metaSet = dao.getWalletMetadata();
            List<DigitalBadge> tableList = new ArrayList<>();
            tableList.addAll(metaSet);
            TableModel tableModel = TableModelCreator.createTableModel(DigitalBadge.class, tableList);
            table1.setModel(tableModel);

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

}
