
## Exercice Suivant: 3.1

# Interfaces Graphiques, Clients lourds et Injection
 ++ Une application fenêtrée avec Swing, des patterns comme MVC, ou encore IOC avec l'injection Spring

## Contexte
* Un premier investisseur s'est intéressé au projet, et il souhaite pouvoir consulter rapidement un prototype sur un appareil quel qu'il soit.
* Nous leur proposons de générer un petit programme s'exécutant sur un de nos PCs pour une démo
* Pour ce faire, nous allons développer un nouveau module, badge-gui, se reposant sur badges-wallet comme dépendance, et y exploiter la technologie Spring...
* Par ailleurs, notre équipe va s'installer dans une ferme d'entreprises où les postes de travail sont tous déjà préconfigurés. Le hic, c'est que l'on a aucun droit pour installer quoi que ce soit, et le seul IDE accessible est IntelliJ en version communautaire. Ceux qui sont en télétravail vont devoir s'aligner sur cet outil en l'installant et en l'utilisant. Ce qui est plutôt plaisant, c'est que cela semble être plutôt efficace, au moins autant qu'éclipse en tout cas...


## Objectifs
* Mises en application:
 - [x] (Exercice 1) Démarrage en 2 temps, affichage rapide de notre liste de métadonnées de Badge, puis customisation fine de la liste ...
    - [ ] Commencer, avec un tutoriel, à afficher simplement la liste des badges (métadonnées), en s'aidant d'une classe d'introspection fournie
    - [ ] Implémenter plus sérieusement un modèle de tableau avec des comportements d'affichage des cellules/lignes selon certains critères.   
 - [ ] (Exercice 2) Permettre l'affichage d'un badge, lors d'un évènement de double-clic sur une ligne
 - [ ] (Exercice 3) Permettre l'ajout d'un badge et la mise à jour visuellement du tableau à l'aide du pattern Observable/Observer
 - [ ] (Exercice 4) Appliquer le pattern IOC avec Spring pour injecter le DAO, et appliquer ce pattern également sur le projet badges-wallet 
----

## Une Première interface graphique Java avec Swing

### Démarrage rapide avec les Forms

 - [ ] Ouvrir le projet existant dans IntelliJ, puis créer un nouveau module au sein de ce projet, appelons le badges-gui.
 - [ ] A partir de cette simple vidéo, reproduire à l'identique (dans notre nomenclature de package a nous toutefois) l'application ainsi présentée.
   - [ ] https://www.youtube.com/watch?v=5vSyylPPEko
 - [ ] Ensuite, ajouter un composant JTable au-dessus du bouton, et donnez-lui un contenu à l'aide de notre DAO et de cette classe d'introspection trouvée sur le Net.
   - [ ] Dans le paramètres du module, ajouter le module badges-wallet en tant que dépendance.
   - [ ] Classe utile: [TableModelCreator.java](help/badges-gui/src/main/java/fr/cnam/foad/nfa035/badges/gui/TableModelCreator.java)
   - [ ] A présent, le construteur de notre composant devrait ressembler à ceci:
```java
public BadgeWalletGUI() {
    button1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            JOptionPane.showMessageDialog(null, "Bien joué!");
        }
    });

    DirectAccessBadgeWalletDAO dao = null;

    try {
        dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
        Set<DigitalBadge> metaSet = dao.getWalletMetadata();
        List<DigitalBadge> tableList = new ArrayList<>();
        tableList.addAll(metaSet);
        TableModel tableModel = TableModelCreator.createTableModel(DigitalBadge.class, tableList);
        table1.setModel(tableModel);

    } catch (IOException ioException) {
        ioException.printStackTrace();
    }
}
```
   - [ ] Et le résultat très rapidement:
![Premiere-IHM][Premiere-IHM]
   - [ ] A vous de jouer, mettez votre preuve en image dans le dossiers screenshots ;)  

### Implémentation réelle

Le première démo s'est bien déroulée, c'est vraiment bien parce que du coup on va pouvoir profiter des locaux de l'incubateur d'entreprise pendant un bail.
Entammons donc notre premier cas:

Donc voilà le topo, on voudrait avoir de belle colonne, cette façon, dans cet ordre:

 * l'ID du badge
 * Son Code série
 * Sa date de début
 * Sa date de fin
   * Si celle-ci est dépassée, afficher la ligne entière en texte rouge
 * Sa taille en octets, 
   * sur fond Orange (cellule uniquement) si > 10 Ko
   * Avec une adaptation de la mesure à l'échelle... et pour cela prendre cette méthode venant du Net:
   ```java
       private String humanReadableByteCountBin(long bytes) {
        long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        if (absB < 1024) {
            return bytes + " o";
        }
        long value = absB;
        CharacterIterator ci = new StringCharacterIterator("KMGTPE");
        for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
            value >>= 10;
            ci.next();
        }
        value *= Long.signum(bytes);
        return String.format("%.1f %co", value / 1024.0, ci.current());
    }
   ```
   
 - [ ] Il s'agit donc d'effectuer ce codage, n'hésitez pas à vous aider du Net si besoin, et aussi aidez-vous donc de ce diagramme qui en soit pourrait suffire :
    - [ ] 
      ```plantuml
      @startuml
      
      title __GUI's Class Diagram__\n
      
        namespace fr.cnam.foad.nfa035.badges.gui {
          class fr.cnam.foad.nfa035.badges.gui.BadgeSizeCellRenderer {
              ~ originBackground : Color
              ~ originForeground : Color
              + BadgeSizeCellRenderer()
              + getTableCellRendererComponent()
              - humanReadableByteCountBin()
          }
        }
        
      
        namespace fr.cnam.foad.nfa035.badges.gui {
          class fr.cnam.foad.nfa035.badges.gui.BadgeWalletG1 {
              {static} - RESOURCES_PATH : String
              - button1 : JButton
              - panel1 : JPanel
              - table1 : JTable
              + BadgeWalletGUI()
              + getIcon()
              {static} + main()
          }
        }
        
      
        namespace fr.cnam.foad.nfa035.badges.gui {
          class fr.cnam.foad.nfa035.badges.gui.BadgesModel {
              - badges : List<DigitalBadge>
              - entetes : String[]
              {static} - serialVersionUID : long
              + BadgesModel()
              + getBadges()
              + getColumnClass()
              + getColumnCount()
              + getColumnName()
              + getRowCount()
              + getValueAt()
          }
        }
        
      
        namespace fr.cnam.foad.nfa035.badges.gui {
          class fr.cnam.foad.nfa035.badges.gui.DefaultBadgeCellRenderer {
              ~ originForeground : Color
              + DefaultBadgeCellRenderer()
              + getTableCellRendererComponent()
          }
        }
        
      
        fr.cnam.foad.nfa035.badges.gui.BadgeSizeCellRenderer -up-|> javax.swing.table.DefaultTableCellRenderer
        fr.cnam.foad.nfa035.badges.gui.BadgesModel -up-|> javax.swing.table.AbstractTableModel
        fr.cnam.foad.nfa035.badges.gui.DefaultBadgeCellRenderer -up-|> javax.swing.table.DefaultTableCellRenderer
      
      @enduml
      ```
   - [ ] Autre source: https://thierry-leriche-dessirier.developpez.com/tutoriels/java/afficher-tableau-avec-tablemodel-5-min/
 - [ ] De nouveau, apportez une preuve de ce genre : ![Deuxieme-IHM][Deuxieme-IHM]



----

[Premiere-IHM]: screenshots/Permiere-IHM.png "Preuve 1"
[Deuxieme-IHM]: screenshots/Deuxieme-IHM.png "Preuve 2"

